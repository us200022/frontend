import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgSelectModule } from '@ng-select/ng-select';
import { NgMultiSelectDropDownModule } from 'ng-multiselect-dropdown';
import { AccordionModule } from 'ngx-bootstrap/accordion';
import { AlertModule } from 'ngx-bootstrap/alert';
import { ModalModule } from 'ngx-bootstrap/modal';
import { PopoverModule } from 'ngx-bootstrap/popover';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { CollapseModule } from 'ngx-bootstrap/collapse';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CollegeUserDashboardComponent } from './components/college-user-dashboard/college-user-dashboard.component';
import { CollegeDetailFormComponent } from './components/college/college-detail-form/college-detail-form.component';
import { CollegeDetailViewComponent } from './components/college/college-detail-view/college-detail-view.component';
import { CompanyDetailFormComponent } from './components/company/company-detail-form/company-detail-form.component';
import { CompanyDetailViewComponent } from './components/company/company-detail-view/company-detail-view.component';
import { LocationComponent } from './components/dropdowns/location/location.component';
import { QualificationComponent } from './components/dropdowns/qualification/qualification.component';
import { SkillsetComponent } from './components/dropdowns/skillset/skillset.component';
import { HomePageComponent } from './components/home-page/home-page.component';
import { JobDetailFormComponent } from './components/job/job-detail-form/job-detail-form.component';
import { JobDetailViewComponent } from './components/job/job-detail-view/job-detail-view.component';
import { LandingPageComponent } from './components/landing-page/landing-page.component';
import { LoginComponent } from './components/login/login.component';
import { RegisterComponent } from './components/register/register.component';
import { RegisterCollegeComponent } from './components/user-dashboard/register-college/register-college.component';
import { RegisterCompanyComponent } from './components/user-dashboard/register-company/register-company.component';
import { SearchCollegeComponent } from './components/user-dashboard/search-college/search-college.component';
import { SearchCompanyComponent } from './components/user-dashboard/search-company/search-company.component';
import { UserDashboardComponent } from './components/user-dashboard/user-dashboard.component';
import { UserDetailFormComponent } from './components/user/user-detail-form/user-detail-form.component';
import { UserDetailViewComponent } from './components/user/user-detail-view/user-detail-view.component';
import { CollegeProfileComponent } from './components/college-user-dashboard/college-profile/college-profile.component';
import { UserProfileComponent } from './components/user/user-profile/user-profile.component';
import { DismissiveAlertComponent } from './components/utils/dismissive-alert/dismissive-alert.component';
import { LoaderComponent } from './components/utils/loader/loader.component';
import { CompanyUserDashboardComponent } from './components/company-user-dashboard/company-user-dashboard.component';
import { CompanyProfileComponent } from './components/company-user-dashboard/company-profile/company-profile.component';
import { CompanyJobsDetailComponent } from './components/company-user-dashboard/company-jobs-detail/company-jobs-detail.component';
import { CollegeJobsDetailComponent } from './components/college-user-dashboard/college-jobs-detail/college-jobs-detail.component';
import { BasicAuthHtppInterceptorService } from './services/authenication/basic-auth-interceptor.service';

@NgModule({
  declarations: [
    AppComponent,
    LandingPageComponent,
    LoginComponent,
    RegisterComponent,
    HomePageComponent,
    JobDetailViewComponent,
    JobDetailFormComponent,
    QualificationComponent,
    SkillsetComponent,
    LocationComponent,
    CollegeDetailViewComponent,
    CollegeDetailFormComponent,
    CompanyDetailFormComponent,
    CompanyDetailViewComponent,
    UserDetailViewComponent,
    UserDetailFormComponent,
    UserDashboardComponent,
    RegisterCollegeComponent,
    RegisterCompanyComponent,
    SearchCompanyComponent,
    SearchCollegeComponent,
    CollegeUserDashboardComponent,
    CollegeProfileComponent,
    CompanyUserDashboardComponent,
    CompanyJobsDetailComponent,
    CompanyProfileComponent,
    UserProfileComponent,
    DismissiveAlertComponent,
    LoaderComponent,
    CollegeJobsDetailComponent,
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    NgMultiSelectDropDownModule.forRoot(),
    NgSelectModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    TooltipModule.forRoot(),
    AccordionModule.forRoot(),
    ModalModule.forRoot(),
    PopoverModule.forRoot(),
    AlertModule.forRoot(),
    TabsModule.forRoot(),
    CollapseModule.forRoot(),
  ],
  providers: [
    {  
      provide:HTTP_INTERCEPTORS, useClass:BasicAuthHtppInterceptorService, multi:true 
    }
    ],
  bootstrap: [AppComponent],
})
export class AppModule {}
