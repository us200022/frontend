import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { CollegeService } from 'src/app/services/college/college.service';
import { CompanyService } from 'src/app/services/company/company.service';
import { College } from 'src/app/shared/models/College.model';
import { Company } from 'src/app/shared/models/Company.model';

@Component({
  selector: 'app-landing-page',
  templateUrl: './landing-page.component.html',
  styleUrls: ['./landing-page.component.css'],
})
export class LandingPageComponent implements OnInit {
  companies!: Company;
  college!: College;

  constructor(
    private router: Router,
    companyService: CompanyService,
    collegeService: CollegeService
  ) {}

  ngOnInit(): void {
  }

  goToHomePage() {
    this.router.navigate(['']);
  }
  goToLoginPage(): void {
    this.router.navigate(['login']);
  }
  goToRegisterPage() {
    this.router.navigate(['register']);
  }
}
