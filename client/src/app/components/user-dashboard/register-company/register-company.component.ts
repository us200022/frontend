import { Component, OnInit, ViewChild } from '@angular/core';
import { CompanyService } from 'src/app/services/company/company.service';
import { Company } from 'src/app/shared/models/Company.model';
import { DismissiveAlertComponent } from '../../utils/dismissive-alert/dismissive-alert.component';

@Component({
  selector: 'app-register-company',
  templateUrl: './register-company.component.html',
  styleUrls: ['./register-company.component.css']
})
export class RegisterCompanyComponent implements OnInit {

  company: Company = {
    companyName: '',
    companyDescription: '',
    companyWebsite: '',
    companyCin: '',
    companyImage: ''};
  
  buttonNameRegister: string = "Register";

  @ViewChild(DismissiveAlertComponent)
  alert!: DismissiveAlertComponent;

  constructor(private companyService: CompanyService) { }

  ngOnInit(): void {
  }

  onUpdate(company: any) {
    console.log(company);
    this.companyService.registerCompany(this.company).subscribe({
      next: (result) => {
        this.company = result;
        this.alert.add('success', 'Company details registered sucessfully', 2000);
      },
      error: (err) => {
        this.alert.add('danger', 'Company details could not be registered', 3000);
      },
      complete: () => console.info('complete'),
    });
  }
}
