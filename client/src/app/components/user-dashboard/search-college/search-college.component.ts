import { Component, OnInit } from '@angular/core';
import { College } from 'src/app/shared/models/College.model';

@Component({
  selector: 'app-search-college',
  templateUrl: './search-college.component.html',
  styleUrls: ['./search-college.component.css'],
})
export class SearchCollegeComponent implements OnInit {
  // List of all companies | Dummy object for testing
  colleges: College[];

  constructor() {}

  ngOnInit(): void {
    // this.collegeService.getAllColleges().subscribe({
    //   next: (result) => {
    //     this.colleges = result;
    //   },
    //   error: (err) => {
    //     console.log(err);
    //   },
    //   complete: () => console.info('complete')
    // })
  }

  userRequestCollege(college: College) {
    // Need change in backend! (A table to store approval requests?)
    alert('Applied for ' + college.collegeName);
  }
}
