import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-user-dashboard',
  templateUrl: './user-dashboard.component.html',
  styleUrls: ['./user-dashboard.component.css'],
})
export class UserDashboardComponent implements OnInit {
  isRegisterCompanyCollapsed: boolean = true;
  isRegisterCollegeCollapsed: boolean = true;
  constructor() {}

  ngOnInit(): void {}
}
