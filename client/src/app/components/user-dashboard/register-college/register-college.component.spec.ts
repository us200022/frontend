import { ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterCollegeComponent } from './register-college.component';

describe('RegisterCollegeComponent', () => {
  let component: RegisterCollegeComponent;
  let fixture: ComponentFixture<RegisterCollegeComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ RegisterCollegeComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterCollegeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
