import { Component, OnInit, ViewChild } from '@angular/core';
import { CollegeService } from 'src/app/services/college/college.service';
import { College } from 'src/app/shared/models/College.model';
import { DismissiveAlertComponent } from '../../utils/dismissive-alert/dismissive-alert.component';

@Component({
  selector: 'app-register-college',
  templateUrl: './register-college.component.html',
  styleUrls: ['./register-college.component.css'],
})
export class RegisterCollegeComponent implements OnInit {
  college: College = {
    collegeName: '',
    collegeWebsite: '',
    collegeNirfRanking:0,
    collegeAicteAffiliation: false,
    collegeLocation: {
      locationCity: '',
      locationCountry: '',
      locationDistrict: '',
      locationPinCode: '',
      locationState: '',
    },
    collegeEmail: '',
  };

  buttonNameRegister: string = 'Register';

  @ViewChild(DismissiveAlertComponent)
  alert!: DismissiveAlertComponent;

  constructor(private collegeService: CollegeService) {}

  ngOnInit(): void {}

  onUpdate(college: any) {
    alert('Works!');
    this.collegeService.registerCollege(this.college).subscribe({
      next: (result) => {
        this.college = result;
        this.alert.add(
          'success',
          'College details registered sucessfully',
          2000
        );
      },
      error: (err) => {
        this.alert.add(
          'danger',
          'Company details could not be registered',
          3000
        );
      },
      complete: () => console.info('complete'),
    });
  }
}
