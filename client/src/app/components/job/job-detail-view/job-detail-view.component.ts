import {
  Component,
  EventEmitter,
  OnInit,
  ViewChild,
  Input,
  Output,
} from '@angular/core';
import { Job } from 'src/app/shared/models/Job.model';
import { JobService } from 'src/app/services/job/job.service';
import { UserService } from 'src/app/services/user/user.service';
import { User } from 'src/app/shared/models/User.model';
import { filter, Subscription } from 'rxjs';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { Router, ActivatedRoute } from '@angular/router';
import { NavigationExtras } from '@angular/router';

import { UserType } from 'src/app/shared/enums/UserType-enum.model';
import { Company } from 'src/app/shared/models/Company.model';
import { JobDomain } from 'src/app/shared/enums/JobDomain-enum.model';
import { JobType } from 'src/app/shared/enums/JobType-enum.model';
import { jobOpenFor } from 'src/app/shared/enums/JobOpenFor-enum.model';
import { Location } from 'src/app/shared/models/Location.model';
import { Qualification } from 'src/app/shared/models/Qualification.model';
import { SkillSet } from 'src/app/shared/models/SkillSet.model';

@Component({
  selector: 'app-job-detail-view',
  templateUrl: './job-detail-view.component.html',
  styleUrls: ['./job-detail-view.component.css'],
})
export class JobDetailViewComponent implements OnInit {
  @Input()
  job!: Job;

  Status = '';
  appliedmessage: any;
  alreadyapplied: any;
  applied: any = [];
  nojobs: any;
  posted: any = [];
  deleted: any;
  errormsg: any;
  successmsg: boolean;
  jobinfo: any;
  jobdata: any = {};
  ProfileForm: FormGroup;
  updated: any;
  //jobs:Job[];
  user: User;

  headers = [
    'Role',
    'Description',
    'Type',
    'Domain',
    'Compensation',
    'Open For',
    'Location',
    'Company',
  ];

  public routeSub: Subscription;
  constructor(
    private activeroute: ActivatedRoute,
    public userService: UserService,
    private jobService: JobService,
    public router: Router,
    private route: ActivatedRoute
  ) {}

  ngOnInit(): void {}

  apply(jobapply: any) {
    this.Status = 'Applied!';
    console.log(jobapply);
    this.jobService.applyjob(jobapply).subscribe((response: any) => {
      if (response.status && response.status == 1) {
        //console.log(response);
        //
        this.appliedmessage = response.message;
        setTimeout(() => {
          this.appliedmessage = '';
          //this. getjobs();
        }, 2000);
      } else {
        this.alreadyapplied = response.message;
        setTimeout(() => {
          this.alreadyapplied = '';
          //this.getjobs();
        }, 1000);
      }
    });
  }

  appliedjobs() {
    this.jobService
      .getappliedjobsbyusers(this.user)
      .subscribe((response: any) => {
        if (response.status && response.status === 1)
          this.nojobs = response.message;
        else {
          this.applied = response;
          this.successmsg = true;
          console.log(this.applied);
        }
      });
  }
  //this.user.userId
  postedjobs() {
    this.jobService.getpostedjobs().subscribe(
      (response: any) => {
        if (response.status && response.status === 1)
          this.nojobs = response.message;
        else {
          this.posted = response;
          this.successmsg = true;
          //console.log(this.applied);
        }
      },
      (error) => {
        this.errormsg = error;
      }
    );
  }

  updatejob(job: any) {
    const navigationExtras: NavigationExtras = {
      state: {
        id: job.id,
        jobRole: job.jobRole,
        jobDescription: job.jobDescription,
        jobType: job.jobType,
        jobDomain: job.jobDomain,
        jobOpenFor: job.jobOpenFor,
        jobLocation: job.jobLocation,
        qualification: job.qualification,
        skillSet: job.skillSet,
        companyUser: job.companyUser,
      },
    };
    this.router.navigate(['editjobs'], navigationExtras);
  }

  deleteJob(job: any): void {
    this.jobService.deleteJob(job.id).subscribe((res) => {
      alert('Employee Deleted');
    });
  }

  getjobsbyparams(jobOpenFor: any) {
    this.jobService.getJobsbyParams(Map).subscribe((data) => {
      //this.jobs = data;
    });
  }
}
