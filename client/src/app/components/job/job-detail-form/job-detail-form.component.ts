import { Component, Input, OnInit, Output, EventEmitter } from '@angular/core';
import { Job } from 'src/app/shared/models/Job.model';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { JobService } from 'src/app/services/job/job.service';
import { Router } from '@angular/router';
import { JobDomain } from 'src/app/shared/enums/JobDomain-enum.model';
import { JobType } from 'src/app/shared/enums/JobType-enum.model';
import { jobOpenFor } from 'src/app/shared/enums/JobOpenFor-enum.model';
import { IDropdownSettings } from 'ng-multiselect-dropdown';
import { NgSelectModule } from '@ng-select/ng-select';

@Component({
  selector: 'app-job-detail-form',
  templateUrl: './job-detail-form.component.html',
  styleUrls: ['./job-detail-form.component.css'],
})
export class JobDetailFormComponent implements OnInit {
  @Input()
  job!: Job;

  formjob: Job;
  postjobForm: FormGroup;
  postedMsg: any;
  alreadyposted: any;
  jobinfo: any;
  jobdata: any = {};
  ProfileForm: FormGroup;
  updated: any;
  routeState: any;

  @Output()
  formSubmit: EventEmitter<any> = new EventEmitter<any>();

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private jobService: JobService
  ) {}

  updateForm: FormGroup;
  categories = [
    { locationId: 1, locationCity: 'Haryana' },
    { locationId: 2, locationCity: 'Bangalore' },
    { locationId: 3, locationCity: 'Pune' },
    { locationId: 4, locationCity: 'Mumbai' },
    { locationId: 5, locationCity: 'Bhubaneswar' },
    { locationId: 6, locationCity: 'Delhi' },
  ];
  selected: any;
  multiple: boolean;
  getSelectedValue() {
    console.log(this.selected);
  }
  categories_skills = [
    { skillSetId: 1, skillSetName: 'C' },
    { skillSetId: 2, skillSetName: 'C++' },
    { skillSetId: 3, skillSetName: 'Java' },
    { skillSetId: 4, skillSetName: 'Python' },
    { skillSetId: 5, skillSetName: 'DSA' },
    { skillSetId: 6, skillSetName: 'Spring Boot' },
  ];
  selected_skills: any;
  multiple_skills: boolean;
  getSelectedSkills() {
    console.log(this.selected_skills);
  }
  // categories_qualifications = [
  //   { id: 1, name: 'B.tech' },
  //   { id: 2, name: 'M.tech' },
  //   { id: 3, name: 'BCA' },
  //   { id: 4, name: 'MCA' },
  // ];
  categories_qualifications = [
    {
      qualificationId: 1,
      qualificationName: 'B.tech',
      qualificationSpecialization: '',
    },
    {
      qualificationId: 2,
      qualificationName: 'M.tech',
      qualificationSpecialization: '',
    },
    {
      qualificationId: 3,
      qualificationName: 'BCA',
      qualificationSpecialization: '',
    },
    {
      qualificationId: 4,
      qualificationName: 'MCA',
      qualificationSpecialization: '',
    },
  ];
  selected_qualifications: any;
  multiple_qualifications: boolean;
  getSelectedQualifications() {
    console.log(this.selected_skills);
  }

  ngOnInit() {
    //this.jobs=history.state;
    this.postjobForm = this.formBuilder.group({
      id: null,
      companyUser: [''],
      jobRole: [''],
      jobDescription: [''],
      jobDomain: [''],
      jobType: [''],
      jobOpenFor: [''],
      jobLocation: [''],
      qualification: [''],
      skillSet: [''],
    });

    if (this.job) {
      this.postjobForm.patchValue(this.job);
    }
  }
  onFormSubmit(): void {
    this.formSubmit.emit(this.postjobForm);
  }

  // compareQualification(o1: any, o2: any) {
  //   if(o1.qualificationId == o2.qualificationId )
  //   return true;
  //   else return false
  // }

  compareQualification(item: any, selected: any) {
    return item.qualificationId === selected.qualificationId;
  }

  compareSkill(item: any, selected: any) {
    return item.skillSetId === selected.skillSetId;
  }

  compareLocation(item: any, selected: any) {
    return item.locationId === selected.locationId;
  }
}
