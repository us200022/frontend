import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';
import { JobService } from 'src/app/services/job/job.service';
import { Job } from 'src/app/shared/models/Job.model';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

@Component({
  selector: 'app-company-jobs-detail',
  templateUrl: './company-jobs-detail.component.html',
  styleUrls: ['./company-jobs-detail.component.css'],
})
export class CompanyJobsDetailComponent implements OnInit {
  jobs: Job[];
  formjob!: Job;
  Status = '';
  appliedmessage: any;
  alreadyapplied: any;
  applied: any = [];
  nojobs: any;
  //posted:any =[];
  deleted: any;
  errormsg: any;
  successmsg: boolean;
  jobinfo: any;
  jobdata: any = {};
  updated: any;
  ProfileForm: FormGroup;
  id: number;
  // public isCollapsed = -1;
  editJob: number = -1;
  postjobForm: FormGroup;
  postedMsg: any;
  alreadyposted: any;

  constructor(
    private router: Router,
    private formBuilder: FormBuilder,
    private activeroute: ActivatedRoute,
    private jobService: JobService
  ) {}

  ngOnInit(): void {
    this.postedjobs();
    this.postjobForm = this.formBuilder.group({
      id: null,
      companyUser: [''],
      jobRole: [''],
      jobDescription: [''],
      jobDomain: [''],
      jobType: [''],
      jobOpenFor: [''],
      jobLocation: [''],
      qualification: [''],
      skillSet: [''],
    });

    console.log('hi');
    console.log(this.postjobForm);
    this.ProfileForm = this.formBuilder.group({
      id: null,
      companyUser: [''],
      jobRole: [''],
      jobDescription: [''],
      jobDomain: [''],
      jobType: [''],
      jobOpenFor: [''],
      jobLocation: [''],
      qualification: [''],
      skillSet: [''],
    });
  }

  onPost(job: any) {
    console.log(job.value);
    this.jobService
      .postjob(JSON.stringify(job.value))
      .subscribe((response: any) => {
        if (response.status && response.status == 1) {
          this.postedMsg = response.message;
          setTimeout(() => {
            this.postedMsg = '';
            // this.router.navigate(['rdashboard/postedjobs']);
          }, 2000);
        } else {
          this.alreadyposted = response.message;
          setTimeout(() => {
            this.alreadyposted = '';
          }, 2000);
        }
      });
  }

  postedjobs() {
    this.jobService.getpostedjobs().subscribe(
      (response: any) => {
        if (response.status && response.status === 1)
          this.nojobs = response.message;
        else {
          this.jobs = response;
          this.successmsg = true;
          //console.log(this.applied);
        }
      },
      (error) => {
        this.errormsg = error;
      }
    );
  }

  onSave(job: Job, i: number) {
    this.editJob = -1;
  }

  onedit(job: any, i: number) {
    this.editJob = i;

    console.log('Edit job is working!');
    console.log(job);

    console.log('hi');
    console.log(this.postjobForm.value);
  }

  onEdit(formjob: any) {
    const data = formjob.value;
    const id = formjob.value.id;
    console.log('on eidt event call', data, id);
    this.jobService.updatejob(data, id).subscribe({
      next: (response: any) => {
        console.log(response);
      },
    });
  }

  onDelete(job: any) {
    console.log('Delete job is working!');
    this.jobService.deleteJob(job.id).subscribe((res) => {
      alert('Employee Deleted');
    });
  }
}
