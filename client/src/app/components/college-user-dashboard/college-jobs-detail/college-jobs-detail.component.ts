import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { JobService } from 'src/app/services/job/job.service';
import {
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';
import { User } from 'src/app/shared/models/User.model';
import { Job } from 'src/app/shared/models/Job.model';

@Component({
  selector: 'app-college-jobs-detail',
  templateUrl: './college-jobs-detail.component.html',
  styleUrls: ['./college-jobs-detail.component.css'],
})
export class CollegeJobsDetailComponent implements OnInit {
  jobs: Job[];
  Status = '';
  appliedmessage: any;
  alreadyapplied: any;
  applied: any = [];
  nojobs: any;
  posted: any = [];
  deleted: any;
  errormsg: any;
  successmsg: boolean;
  jobinfo: any;
  jobdata: any = {};
  ProfileForm: FormGroup;
  updated: any;
  //jobs:Job[];
  user: User;

  constructor(
    private router: Router,
    private activeroute: ActivatedRoute,
    private jobService: JobService
  ) {}

  ngOnInit(): void {
    this.getjobs();
  }

  getjobs() {
    this.jobService.getjobList().subscribe((data) => {
      this.jobs = data;
    });
  }

  onApply(job: any) {
    this.Status = 'Applied!';
    console.log(job);
    this.jobService.applyjob(job).subscribe((response: any) => {
      if (response.status && response.status == 1) {
        //console.log(response);
        //
        this.appliedmessage = response.message;
        setTimeout(() => {
          this.appliedmessage = '';
          //this. getjobs();
        }, 2000);
      } else {
        this.alreadyapplied = response.message;
        setTimeout(() => {
          this.alreadyapplied = '';
          //this.getjobs();
        }, 1000);
      }
    });
  }

  appliedjobs() {
    this.jobService
      .getappliedjobsbyusers(this.user)
      .subscribe((response: any) => {
        if (response.status && response.status === 1)
          this.nojobs = response.message;
        else {
          this.applied = response;
          this.successmsg = true;
          console.log(this.applied);
        }
      });
  }
}
