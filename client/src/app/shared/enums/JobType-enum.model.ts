export enum JobType {
  FULL_TIME,
  PART_TIME,
  INTERN
}