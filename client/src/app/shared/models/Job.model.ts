import { JobDomain } from '../enums/JobDomain-enum.model';
import { jobOpenFor } from '../enums/JobOpenFor-enum.model';
import { JobType } from '../enums/JobType-enum.model';
import { Location } from './Location.model';
import { Qualification } from './Qualification.model';
import { SkillSet } from './SkillSet.model';
import { User } from './User.model';

export interface Job {
  jobId?: number;
  jobRole: string;
  jobDescription: string;
  jobCompensation: number;
  jobLocation: Location[];
  jobDomain: JobDomain;
  jobType: JobType;
  jobOpenFor: jobOpenFor;
  qualification: Qualification[];
  skillSet: SkillSet[];
  jobCompanyUser: User;
}

//js
//typecript-->transpile--->js--->exceute
