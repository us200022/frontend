export const ServerConfig = {
  serverBaseUrl: 'http://localhost:8080/',

  //Auth
  loginAuthEndpoint: 'api/auth/login',
  logoutAuthEndpoint: 'logout',
  registerAuthEndpoint: 'api/auth/register',

  //College Admin or Company Admin
  collegeUsersAdminEndpoint: 'api/admin/college',
  companyUsersAdminEndpoint: 'api/admin/company',
  approveCompanySpocAdminEndpoint: 'api/admin/company/approval',
  approveCollegeSpocAdminEndpoint: 'api/admin/college/approval',

  //College Spoc and Company Spoc apply
  applyCollegeEndpoint: 'api/apply/college',
  applyCompanyEndpoint: 'api/apply/company',

  userEndpoint: 'api/users',

  collegeEndpoint: 'api/colleges',

  companyEndpoint: 'api/companies',

  //jobposts
  jobPostEndpoint: 'api/jobposts',
  jobPostByCompanyEndpoint: 'api/jobposts/company',
  jobPostByCompanyUserEndpoint: 'api/jobposts/company/user',
  jobApplyByCollegeEndpoint:'api/jobpost/applycolleges',

  locationEndpoint: 'api/locations',

  skillsetEndpoint: 'api/skillsets',

  qualificationEndpoint: 'api/qualifications',

  //College Apply for JobPosts
  applyJobsByCollegeEndpoint:'api/college/applyjobs'
};
