import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { User } from 'src/app/shared/models/User.model';
import { ServerConfig } from 'src/app/shared/server-config';
import { Observable } from 'rxjs';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root',
})
export class AuthenticationService {
  constructor(private httpClient: HttpClient,private router: Router) {}

  authenticate(email: string, password: string): Observable<User> {
    console.log(email);
    console.log(password);
    const headers = new HttpHeaders({
      Authorization: 'Basic ' + btoa(email + ':' + password),
    });
    let payload: any = {
      email: email,
      password: password,
    };
    return this.httpClient
      .post<User>(
        `${ServerConfig.serverBaseUrl}${ServerConfig.loginAuthEndpoint}`,
        payload,
        { headers }
      )
      .pipe(
        map((userData) => {
          sessionStorage.setItem('email', email);
          let authString = 'Basic ' + btoa(email + ':' + password);
          sessionStorage.setItem('basicauth', authString);
          return userData;
        })
      );
  }

  isUserLoggedIn(): boolean {
    let user = sessionStorage.getItem('email');
    console.log(!(user === null));
    return !(user === null);
  }

  logOut(): void {
    sessionStorage.clear();
    this.router.navigate(['login']);
  }
}
