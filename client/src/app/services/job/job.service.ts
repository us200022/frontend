import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { map, Observable } from 'rxjs';
import { Company } from 'src/app/shared/models/Company.model';
import { Job } from 'src/app/shared/models/Job.model';
import { User } from 'src/app/shared/models/User.model';
const PUBLIC = 'http://localhost:3000/jobs/';
const PUBLIC1 = 'http://localhost:3000/appliedjobs';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

@Injectable({
  providedIn: 'root',
})
export class JobService {
  user: User[];
  //job:Job;
  constructor(private http: HttpClient) {}

  getjobList(): Observable<any> { //: Observable<APIResponse<Jobs>>
    const httpOptions = {
      headers: new HttpHeaders({
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
      }),
    };

    return this.http.get(`${PUBLIC}`);
  }
  getjobbyId(jobId: number): Observable<any> { //: Observable<APIResponse<Jobs>>
    const httpOptions = {
      headers: new HttpHeaders({
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
      }),
    };

    return this.http.get(`${PUBLIC}` + jobId);
  }
  applyjob(jobs: any): Observable<any> {
    const jobId = jobs.jobId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    };
    return this.http.post(`${PUBLIC}apply`, jobs, httpOptions);
  }

  //userId:number
  getappliedjobsbyusers(user: User) {
    const userId = user.userAccountId;
    const httpOptions = {
      headers: new HttpHeaders({
        Accept: 'application/json, text/plain, */*',
        'Content-Type': 'application/json',
      }),
    };
    return this.http.get(`${PUBLIC}` + userId?.toString, httpOptions);
    //return this.http.get(`${PUBLIC}/applied/`+userId.toString,httpOptions);
  }

  postjob(jobs: any): Observable<any> {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    };
    return this.http.post(`${PUBLIC}`, jobs, httpOptions);
  }
  //userId:number
  getpostedjobs() {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
        Accept: 'application/json, text/plain, */*',
      }),
    };
    return this.http.get(`${PUBLIC}`, httpOptions);
    // return this.http.get(`${PUBLIC}/company/user/`+userId.toString,httpOptions);
  }

  updatejob(data: any, id: any) {
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    };
    return this.http.put('http://localhost:3000/jobs/' + id, data).pipe(
      map((res: any) => {
        return res;
      })
    );
  }
  deleteJob(jobId: number): Observable<Job> {
    //const jobId=job.jobId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    };
    //+jobId.toString()
    return this.http.delete<any>('http://localhost:3000/jobs/' + jobId).pipe(
      map((res: any) => {
        return res;
      })
    );
  }

  getjobsbycompany(company: Company) {
    const companyId = company.companyId;
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json',
      }),
    };
    return this.http.get(
      `${PUBLIC}/company` + companyId?.toString,
      httpOptions
    );
  }

  getJobsbyParams(Map: any): Observable<any> {
    const url = 'http://localhost:3000/jobs/';

    let queryParams = new HttpParams();
    queryParams.append('jobType', 1);
    queryParams.append('jobDomain', 1);
    queryParams.append('jobLocation', 1);
    queryParams.append('qualification', 1);
    queryParams.append('jobRole', 1);
    queryParams.append('jobOpenFor', 1);

    return this.http.get<any>(url, { params: queryParams });
  }
}
