import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs';
import { College } from 'src/app/shared/models/College.model';
import { Job } from 'src/app/shared/models/Job.model';
import { ServerConfig } from 'src/app/shared/server-config';
import { User } from 'src/app/shared/models/User.model';

const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type': 'application/json',
  }),
};

@Injectable({
  providedIn: 'root',
})
export class CollegeService {
  constructor(private http: HttpClient) {}

  // public getCollegesByName(college: College): Observable<College> {
  //   return this.http.get<College>(`${this.apiUrl}/${college.collegeName}`);
  // }

  // public getCollegeById(collegeId: College["collegeId"]): Observable<College> {
  //   return this.http.get<College>(
  //     `${ServerConfig.serverBaseUrl}${ServerConfig.collegeEndpoint}/${collegeId}`,
  //     httpOptions
  //   );
  // }

  public getCollegeById(collegeId: College['collegeId']): Observable<College> {
    return this.http.get<College>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.collegeEndpoint}/${collegeId}`,
      httpOptions
    );
  }

  public getAllColleges(): Observable<College[]> {
    return this.http.get<College[]>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.collegeEndpoint}`,
      httpOptions
    );
  }

  // public getCollegesByLocation(college: College): Observable<College> {
  //   return this.http.get<College>(`${this.apiUrl}/${college.collegeLocation}`);
  // }

  // public getCollegeByNirfRanking(college: College): Observable<College> {
  //   return this.http.get<College>(`${this.apiUrl}/${college.collegeNirfRanking}`);
  // }

  // public getCollegesSortedByNirfRanking(college: College): Observable<College> {
  //   return this.http.get<College>(this.apiUrl);
  // }

  // public getCollegesByAicteAffiliation(college: College): Observable<College> {
  //   return this.http.get<College>(`${this.apiUrl}/${college.collegeAicteAffiliation}`);
  // }

  // public getAllCollegesCustom(college: College): Observable<College> {
  //   return this.http.get<College>(this.apiUrl);
  // }

  public getAllCollegeUsers(collegeId: number): Observable<User[]> {
    return this.http.get<User[]>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.collegeUsersAdminEndpoint}/${collegeId}`,
      httpOptions
    );
  }

  public registerCollege(college: College): Observable<College> {
    return this.http.post<College>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.collegeEndpoint}`,
      httpOptions
    );
  }

  public updateCollege(college: College): Observable<College> {
    const collegeId = college.collegeId;
    return this.http.put<College>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.collegeEndpoint}/${collegeId}`,
      college,
      httpOptions
    );
  }

  // public deleteCollege(college: College): Observable<College> {
  //   const url = `${this.apiUrl}/${college.collegeId}`;
  //   return this.http.delete<College>(url);
  // }

  public approveSpocApproval(userId: number) {
    return this.http.post<any>(
      `${ServerConfig.serverBaseUrl}${ServerConfig.approveCollegeSpocAdminEndpoint}/${userId}`,
      httpOptions
    );
  }
}
